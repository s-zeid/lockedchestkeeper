NMS class is: net.minecraft.server.1_6_R3

LOCKED_CHEST initialization is:
    public static final Block LOCKED_CHEST = (new BlockLockedChest(95)).c(0.0F).a(1.0F).a(h).c("lockedchest").b(true);

StepSound fields in Block.java:
    public static final StepSound g = new StepSound("stone", 1.0F, 1.0F);
    public static final StepSound h = new StepSound("wood", 1.0F, 1.0F);
    public static final StepSound i = new StepSound("gravel", 1.0F, 1.0F);
    public static final StepSound j = new StepSound("grass", 1.0F, 1.0F);
    public static final StepSound k = new StepSound("stone", 1.0F, 1.0F);
    public static final StepSound l = new StepSound("stone", 1.0F, 1.5F);
    public static final StepSound m = new StepSoundStone("stone", 1.0F, 1.0F);
    public static final StepSound n = new StepSound("cloth", 1.0F, 1.0F);
    public static final StepSound o = new StepSound("sand", 1.0F, 1.0F);
    public static final StepSound p = new StepSound("snow", 1.0F, 1.0F);
    public static final StepSound q = new StepSoundLadder("ladder", 1.0F, 1.0F);
    public static final StepSound r = new StepSoundAnvil("anvil", 0.3F, 1.0F);

BlockLockedChest decay method header is:
    public void a(World world, int i, int j, int k, Random random);

