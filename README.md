LockedChestKeeper CraftBukkit Plugin
====================================
Prevents Locked Chests from decaying.

Copyright (c) 2013 Scott Zeid.  Released under the X11 License.  
<http://code.s.zeid.me/lockedchestkeeper>

[**LockedChestKeeper on BukkitDev**](http://dev.bukkit.org/bukkit-plugins/lockedchests/)

# Minecraft 1.7 replaced the block ID used by locked chests (95) with stained glass.  Therefore, this plugin will no longer be maintained, except to maintain compatibility with minor 1.6 releases.

This plugin prevents locked chests from decaying.

Configuration
-------------

This plugin currently requires no configuration.  None are planned.

All locked chests will be preserved regardless of data value.  This may
change in the future, although this plugin will *never* decay chests if
they have a data value of 0 or 4, even in the future.

Permissions
-----------

This plugin currently does not have any permissions.  None are planned.

Compiling
---------

To manually build the plugin, run `mvn` from the root of the source tree.
You will need a working Internet connection in order for Maven to download
the appropriate dependencies.  The compiled JAR file will be written to
`target/LockedChests-<version>.jar`.
