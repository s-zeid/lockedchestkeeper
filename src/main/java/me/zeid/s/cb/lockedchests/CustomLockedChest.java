/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   LockedChestKeeper CraftBukkit Plugin
   Prevents Locked Chests from despawning.
   
   Copyright (C) 2013 Scott Zeid
   http://code.s.zeid.me/lockedchestkeeper
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.lockedchests;

import java.lang.IllegalAccessException;
import java.lang.NoSuchMethodException;
import java.lang.SecurityException;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Random;

import net.minecraft.server.v1_6_R3.Block;
import net.minecraft.server.v1_6_R3.BlockLockedChest;
import net.minecraft.server.v1_6_R3.StepSound;
import net.minecraft.server.v1_6_R3.World;

public class CustomLockedChest extends BlockLockedChest {
 private Logger logger;
 
 public CustomLockedChest(int i, Logger logger)
        throws IllegalAccessException, InvocationTargetException,
               NoSuchMethodException, SecurityException {
  super(i);
  
  Method m;
  
  m = Block.class.getDeclaredMethod("c", Float.TYPE);
  m.setAccessible(true);
  m.invoke(this, 0.0F);
  
  m = Block.class.getDeclaredMethod("a", Float.TYPE);
  m.setAccessible(true);
  m.invoke(this, 1.0F);
  
  m = Block.class.getDeclaredMethod("a", StepSound.class);
  m.setAccessible(true);
  m.invoke(this, Block.h);

  this.c("lockedchest");
  
  m = Block.class.getDeclaredMethod("b", Boolean.TYPE);
  m.setAccessible(true);
  m.invoke(this, true);
  
  this.logger = logger;
 }
 
 @Override
 public void a(World world, int i, int j, int k, Random random) {
  //logger.log(Level.INFO, "caught locked chest decay; cancelling... : "
  //            + String.format("(%d, %d, %d)", i, j, k));
  return;
 }
}
