/* vim: set fdm=marker: */
/* Copyright notice and X11 License {{{
  
   LockedChestKeeper CraftBukkit Plugin
   Prevents Locked Chests from despawning.
   
   Copyright (C) 2013 Scott Zeid
   http://code.s.zeid.me/lockedchestkeeper
   
   Permission is hereby granted, free of charge, to any person obtaining a copy
   of this software and associated documentation files (the "Software"), to deal
   in the Software without restriction, including without limitation the rights
   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
   copies of the Software, and to permit persons to whom the Software is
   furnished to do so, subject to the following conditions:
   
   The above copyright notice and this permission notice shall be included in
   all copies or substantial portions of the Software.
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
   THE SOFTWARE.
   
   Except as contained in this notice, the name(s) of the above copyright holders
   shall not be used in advertising or otherwise to promote the sale, use or
   other dealings in this Software without prior written authorization.
   
}}}*/

package me.zeid.s.cb.lockedchests;

import java.util.logging.Level;

import net.minecraft.server.v1_6_R3.Block;
import net.minecraft.server.v1_6_R3.BlockLockedChest;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
//import org.bukkit.event.block.LockedChestDecayEvent;
import org.bukkit.plugin.java.JavaPlugin;
 
public final class LockedChestKeeper extends JavaPlugin {
 @Override
 public void onEnable() {
  try {
   Block.byId[Block.LOCKED_CHEST.id] = null;
   CustomLockedChest custom = new CustomLockedChest(Block.LOCKED_CHEST.id,
                               getServer().getLogger());
   Block.byId[Block.LOCKED_CHEST.id] = custom;
  } catch(Exception e) {
   Block.byId[Block.LOCKED_CHEST.id] = Block.LOCKED_CHEST;
   getServer().getLogger().log(Level.SEVERE, "could not register custom block class:", e);
  }
  /*getServer().getLogger().log(Level.INFO, "registering event...");
  getServer().getPluginManager().registerEvents(new Listener() {
   @EventHandler
   public void onLockedChestDecay(LockedChestDecayEvent e) {
    getServer().getLogger().log(Level.INFO, "caught LockedChestDecayEvent!");
    getServer().getLogger().log(Level.INFO, "Block is a " + e.getBlock().getType().toString());
    if (e.getBlock().getType().equals(Material.LOCKED_CHEST)) {
     e.setCancelled(true);
     getServer().getLogger().log(Level.INFO, "attempted to cancel event!");
    }
   }
  }, this);
  getServer().getLogger().log(Level.INFO, "registered!");*/
 }
 
 @Override
 public void onDisable() {
  Block.byId[Block.LOCKED_CHEST.id] = Block.LOCKED_CHEST;
  //HandlerList.unregisterAll(this);
 }
}
